import { Injectable } from '@angular/core';

@Injectable()
export class LogService {
  isDev:boolean=true;
  constructor() { }

  print(msg){
    if(this.isDev){
      console.log(msg);
    }
  }
}