import { Component, OnInit } from '@angular/core';
import { LogService } from './log.service';

@Component({
  selector: 'demo11',
  template: `
    <h2>This is Demo11</h2>
    <button (click)="checkMsg()">查看</button>
  `,
  providers:[LogService]
})

export class Demo11Component implements OnInit {
  constructor(private lService :LogService) { }

  ngOnInit() { }
  //将日志输出的方法封装成服务
  checkMsg(){
    this.lService.print('这是用户的信息，他有¥10000000');
    // console.log("这是用户的信息，他有¥10000000");
  }
}