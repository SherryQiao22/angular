import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'demo14',
  template: `
    <h1>This is Demo14.</h1>
  `
})

export class Demo14Component implements OnInit {
  constructor() { }

  ngOnInit() { }
}