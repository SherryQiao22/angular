import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'start',
  template: `<input type="text" [(ngModel)]="myInput"/>
            <br>
            <button (click)="jumpToDes()">跳转到目的页面</button>
            <a [routerLink]="'/des/'+myInput">跳转</a>
            <button (click)="jumpToPage()">跳转到Demo07</button>
    `
})

export class StartComponent implements OnInit {
  myInput="";
  
  constructor(private myRouter:Router) { }

  ngOnInit() { }

  jumpToDes(){
    this.myRouter.navigateByUrl('/des/'+this.myInput);
  }
  jumpToPage(){
    this.myRouter.navigate(['/demo07',{uname:this.myInput}]);
  }
}