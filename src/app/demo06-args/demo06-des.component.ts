import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'des',
  template: `
    <h2>接收方页面</h2>
    <p>{{"接受到的参数为"+rcvMsg}}</p>
  `
})

export class DesComponent implements OnInit {
  rcvMsg='';
  constructor(private aRoute:ActivatedRoute) { }

  ngOnInit() {
    //接收参数
    this.aRoute.params.subscribe((data)=>{
      this.rcvMsg=data.uname;
    });
  }
}