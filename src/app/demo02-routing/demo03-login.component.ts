import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'demo03',
  template: `<h4>this is Login Page.</h4>
            <button (click)="goBack()">返回上一页</button>`
})

export class Demo03LoginComponent implements OnInit {
  constructor(private myLocation:Location) { }

  ngOnInit() { }

  goBack(){
    this.myLocation.back();
  }
}