import { Input, Component, OnInit } from '@angular/core';
import { MyHttpService } from '../utilities/myhttp.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-head',
  templateUrl: 'header.component.html',
  styles: [`
    .header {
      height:50px;
      background:#ADADAD;
      line-height:50px;
    }
  `]
})

export class HeaderComponent implements OnInit {

  constructor(private myHttp: MyHttpService) { }

  ngOnInit() {
    console.log("header接受的main传来的uLogined为" + this.hasLogined);

  }

  jumpToUnknown() {
    this.myHttp.sendRequest("https://946c1868-e3cf-4c5a-bd11-01d9677e5ea0.mock.pstmn.io/mock")
      .subscribe((data: any) => {
        console.log("接受到的信息：" + data);
      });
  }
  jumpToUnknown1() {
    this.myHttp.sendRequest("https://946c1868-e3cf-4c5a-bd11-01d9677e5ea0.mock.pstmn.io/mock1")
      .subscribe((data: any) => {
        console.log("接受到的信息：" + data);
      });
  }

  @Input() hasLogined: boolean;
  @Input() username: string;

}