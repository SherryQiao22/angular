import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MyHttpService {
  constructor(private http: Http,private myRouter:Router) { }

  // private handleError(error:any): Promise<any>{
  //   console.error('An error occured',error);
  //   return Promise.reject(error.message || error);
  // }

  sendRequest(myUrl) {
    return this.http
      .get(myUrl)
      .map((response: Response) => response.json())
      //.catch(this.handleError);
      .catch((error: Response) => {
        // switch (error.status) {
        //   case 404: case 500: case 401: }
        this.myRouter.navigate(["/notFound",{status:error.status}]);
        
        console.log(error.status);
        return Observable.throw(error);
      })
  }
}
