import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class CanCheckMailGuard implements CanActivate {
  constructor() { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log(route,state);
    return  this.canCheckMail(route.params.uid);
  }

  canCheckMail(uid){
    //若用户输入为整数且在0-100之间
    if(uid%1===0&&uid>0&&uid<=100){
      return true;
    }else{
      return false;
    }
  }
}