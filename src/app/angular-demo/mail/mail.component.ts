import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'mail',
  template: `
    <h1>Check Your Mail:</h1>
    <ul>
      <li><a routerLink="/mail/inbox">收件箱</a></li>
      <li><a routerLink="/mail/outbox">发件箱</a></li>
      <li><a routerLink="/mail/trash">回收箱</a></li>
    </ul>
    <router-outlet></router-outlet>
    <button (click)="goBack()">返回上一页</button>
  `
})

export class MailComponent implements OnInit {
  constructor(private myLocation:Location) { }

  ngOnInit() { }

  goBack(){
    this.myLocation.back();
  }
}