import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'trash',
  template: `
    <h1>---Trash---</h1>
    <ul>
      <li><a href="javascript:;">邮件1</a></li>
      <li><a href="javascript:;">邮件2</a></li>
      <li><a href="javascript:;">邮件3</a></li>
    </ul>
  `
})

export class TrashComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}