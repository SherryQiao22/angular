import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'outbox',
  template: `
    <h1>---Outbox---</h1>
    <ul>
      <li><a href="javascript:;">已发邮件1</a></li>
      <li><a href="javascript:;">已发邮件2</a></li>
      <li><a href="javascript:;">已发邮件3</a></li>
    </ul>
  `
})

export class OutboxComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}