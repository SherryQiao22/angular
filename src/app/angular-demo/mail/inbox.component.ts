import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'inbox',
  template:`
    <h1>---Inbox---</h1>
    <ul>
      <li><a href="javascript:;">未读邮件1</a></li>
      <li><a href="javascript:;">未读邮件2</a></li>
      <li><a href="javascript:;">未读邮件3</a></li>
    </ul>
  ` 
})

export class InboxComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}