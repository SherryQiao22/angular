import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'to-login',
  templateUrl: 'login.component.html'
})

export class ToLoginComponent implements OnInit {
  name:'';
  pwd:'';
  isNameNull:boolean;
  isPwdNull:boolean;
  constructor(private myRouter:Router) { }

  ngOnInit() { }

  //跳转到main页面，将用户名传递给main页面
  loginToMain(){
    if(this.name&&this.pwd){
      this.myRouter.navigate(['/main',{name:this.name}]);
    }
    if(!this.name){
      this.isNameNull=true;
    }
    if(!this.pwd){
      this.isPwdNull=true;
    }
  }
}