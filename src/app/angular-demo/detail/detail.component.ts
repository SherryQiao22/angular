import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'detail',
  templateUrl: 'detail.component.html',
  styles:[`
    .floatBox>div{
      float:left;
      width:30%;
    }
  `]
})

export class DetailComponent implements OnInit {
  rcvInfo:any;
  money:any;

  
  constructor(private myRouter:Router,private aRoute:ActivatedRoute,private myLocation:Location) { }

  ngOnInit() { 
    this.aRoute.params.subscribe((data:any)=>{
      this.rcvInfo=data;
    });
    this.money=Math.random()*1000+100;
  }

  goBack(){
    this.myLocation.back();
  }
  
}