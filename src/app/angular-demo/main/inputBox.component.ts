import { Input,Output,EventEmitter,Component, OnInit } from '@angular/core';

@Component({
  selector: 'input-box',
  templateUrl: 'inputBox.component.html'
})

export class InputBoxComponent implements OnInit {
  newUser:any;
  uname:string;
  addr:string;
  constructor() { }

  ngOnInit() { }

  @Input() displayBox="";
  @Output() sendToMain=new EventEmitter();

  confirmToAdd(){
    this.newUser={'title':this.uname,'body':this.addr};
    this.sendToMain.emit(this.newUser);
  }

}