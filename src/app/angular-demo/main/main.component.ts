import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { MyHttpService } from '../utilities/myhttp.service';

@Component({
  selector: 'main',
  templateUrl: `main.component.html`,
  providers:[MyHttpService],
  styles:[`
    .clear:after{
      content:"";
      display:block;
      clear:both;
    }
    .dialog{
      position:absolute;
      left:40%;
      top:20%;
      width:200px;
      height:150px;
      z-index:3;
      background:#F2F2F2;
      border:1px solid #9A9A9A;
      border-radius:10px;
      padding:20px;
    }
    
  `]
})

export class MainComponent implements OnInit {
  userInfo:Array<any>=[{"userId":0,"id":0,"title":"...","body":"..."}];
  userLogined:boolean=false;
  toAddUser:boolean=false;
  cusname:"";
  showDia:boolean=false;
  uid:any;
  currentI:any;
  passObj:any;

  constructor(private aRoute:ActivatedRoute,private myHttp:MyHttpService,private myRouter:Router) { }

  ngOnInit() { 
    //发送网络请求接收数据
    this.myHttp.sendRequest('https://jsonplaceholder.typicode.com/posts')
        .subscribe((data:any)=>{
          //console.log(data[1]);
          this.userInfo=data;
        });
    //接受从login页面传递的name属性
    this.aRoute.params.subscribe((data:any)=>{
      console.log("main接受的从login传来的name为"+data.name);
      if(data.name){
        this.userLogined=true;
        this.cusname=data.name;
      }else{
        this.userLogined=false;
      }
    })
  }

  deleteUser(i){
    console.log(i);
    console.log(this.userInfo);
    this.userInfo.splice(i,1);
    console.log(this.userInfo);
  }
  addUser(){
    this.toAddUser=true;
  }
  //接收从用户输入组件中传来的数据
  rcvInfo(data:any){
    console.log(data);
    this.userInfo.push(data);
    this.toAddUser=false;
  }

  jumpToDetail(){
    this.passObj=this.userInfo[this.currentI];
    this.passObj.uid=this.uid;//uid发送给路由守卫服务用于验证用户身份
    this.myRouter.navigate(['/detail',this.passObj]);//用户信息传递给详情页面，用于显示用户信息
  }
  canCheckMail(i){
    this.showDia=true;
    this.currentI=i;
  }
  cancel(){
    this.showDia=false;
  }
}