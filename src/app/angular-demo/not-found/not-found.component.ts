import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'not-found',
  template: `
    <h1>{{"Response: "+myStatus}}</h1>
    <button (click)="goBack()">返回上一页</button>
  `
})

export class NotFoundComponent implements OnInit {
  myStatus:string;
  constructor(private aRoute:ActivatedRoute,private myLocation:Location) { }

  ngOnInit() {
    this.aRoute.params.subscribe((data:any)=>{
      console.log(data);
      this.myStatus=data.status;
    });
   }
   goBack(){
    this.myLocation.back();
   }
}