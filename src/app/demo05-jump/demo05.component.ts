import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'demo05',
  template: `<h1>this is demo05.</h1>
            <button (click)="jumpToLogin()">跳转到登录页</button>
            <a routerLink="/main">跳转到Main页面</a>
            <br>
            <button routerLink='/login'>去登陆</button>
            <br>
            <button (click)="jump()">去主页面</button>
            <br>
            <button routerLink="/demo01">Demo01-direction</button>
            <button routerLink="/start">Demo06-Args</button>
            <br>
            <button routerLink="/mail">跳转到邮件</button>
            <br>
            <button routerLink="/parent">props down</button>
            <br>
            <button routerLink="/demo10">pipe</button>
            <br>
            <button routerLink="/demo11">Service</button>
            <button routerLink="/demo12">HeartBeatService</button><br>
            <button routerLink="/demo13">HTTPService</button>
            <button routerLink="/demo14">RoutingGuard</button><br>
            <button routerLink="/demo15">CustomDirective</button><br>
            
            `
})

export class Demo05Component implements OnInit {
  constructor(private myRouter:Router) {

   }

  ngOnInit() { }

  jumpToLogin(){
    this.myRouter.navigateByUrl('/login');
  }
  jump(){
    this.myRouter.navigate(['/main']);
  }
}