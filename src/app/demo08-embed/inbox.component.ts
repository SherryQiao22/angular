import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'inbox',
  template: `
    <ul>
      <li>有新的未读邮件1</li>
      <li>有新的未读邮件2</li>
      <li>有新的未读邮件3</li>
    </ul>
  `
})

export class InboxComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}