import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mail',
  template: `
    <a routerLink="/mail/inbox">收件箱</a>
    <a routerLink="/mail/outbox">发件箱</a>
    <router-outlet></router-outlet>
  `
})

export class MailComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}