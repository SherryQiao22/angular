import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'outbox',
  template: `
    <ul>
      <li>已读邮件1</li>
      <li>已读邮件2</li>
      <li>已读邮件3</li>
    </ul>
  `
})

export class OutboxComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}