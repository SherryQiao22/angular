import { Component, OnInit,Output,EventEmitter,Input } from '@angular/core';

@Component({
  selector: 'demo09Son',
  template: `
    <h2>This is son Component.</h2>
    <h2>{{"myName  is "+myName}}</h2>
    <input [(ngModel)]="myMsg" type="text" placeholder="请输入要发给父组件的信息"/>
    <button (click)="sendToFather()">发送</button>
  `
})

export class Demo09SonComponent implements OnInit {
  sonAge=23;
  myMsg="";
  //如果调用该组件时，指定了myName属性，值就会保存在myName变量中
  @Input() myName="";
  @Output() toParentEvent=new EventEmitter();

  constructor() { }

  ngOnInit() { }

  sendToFather(){
    //方法内实现 触发 绑定给 此组件的toParentEvent事件并传递参数
    this.toParentEvent.emit(this.myMsg);
  }
}