import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'demo09Parent',
  template: `
    <h2>This is parent Component.</h2>
    <input type="text" [(ngModel)]="sonName"/>
    <hr>
    <h3>{{"利用本地变量获得的子组件的年龄： "+mySon.sonAge}}</h3>
    <demo09Son #mySon (toParentEvent)="rcvMsg($event)" [myName]="sonName"></demo09Son>
  `
})

export class Demo09ParentComponent implements OnInit {
  sonName="zhangsan";

  constructor() { }

  ngOnInit() { }

  //接受通过事件传递的数据
  rcvMsg(data:any){
    console.log(data);
  }
}