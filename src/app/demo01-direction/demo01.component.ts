import {Component} from '@angular/core';

@Component({
  selector:'demo01',
  templateUrl:'./demo01.component.html',
  styleUrls:['./demo01.css']
})

export class Demo01Component{
  list=[100,200,300,400,500];
  hasMore = true;
  answer= 'd';
  stus=[
    {'stuName':'mike','stuScore':34,'stuAddr':'beijing'},
    {'stuName':'micy','stuScore':74,'stuAddr':'beijing'},
    {'stuName':'milly','stuScore':84,'stuAddr':'beijing'},
    {'stuName':'jason','stuScore':64,'stuAddr':'beijing'},
    {'stuName':'tomi','stuScore':74,'stuAddr':'beijing'}
  ];
  imgUrl:string= "1.png";
  linkUrl:string= "http://baidu.com";
  myColor:string= 'red';
  myOpacity:number= 0;
  isIntervalWork:boolean= false;
  myAddr:string= "";
  myPhone:string= "123";

  changeOpacity(){
    if(this.isIntervalWork){
      return;
    }
    this.isIntervalWork=true;
    //注意this指向
    setInterval(()=>{
      this.myOpacity+=0.1;
      if(this.myOpacity>1){
        this.myOpacity=0;
      }
    },100);
  }

  handleClick(){
    console.log('btn is clicked');
  }

  //输入发生变化时调用的方法
  getUserInput(data:string){
    console.log("当前输入框中的内容为"+this.myPhone);
    console.log(data);
  }
}
