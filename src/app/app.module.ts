import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.router';
import {HttpModule} from '@angular/http';
import { CanEnterGuard } from './demo14-guard/canEnter.service';
import { CanCheckMailGuard } from './angular-demo/utilities/canCheckMail.service';

import { AppComponent } from './app.component';
// import { Demo01Component } from './demo01-direction/demo01.component';
// import { Demo02Component } from './demo02-routing/demo02.component';
// import { Demo03LoginComponent } from './demo02-routing/demo03-login.component';
// import { Demo04MainComponent } from './demo02-routing/demo04-main.component';
// import { Demo05Component } from './demo05-jump/demo05.component';
// import { StartComponent } from './demo06-args/demo06-start.component';
// import { DesComponent } from './demo06-args/demo06-des.component';
// import { Demo07Component } from './demo07-args/demo07.component';
// import { MailComponent } from './demo08-embed/mail.component';
// import { InboxComponent } from './demo08-embed/inbox.component';
// import { OutboxComponent } from './demo08-embed/outbox.component';
// import { Demo09ParentComponent}  from './demo09-communication/demo09-parent.component';
// import { Demo09SonComponent}  from './demo09-communication/demo09-son.component';
// import { Demo10Component } from './demo10-pipe/demo10.component';
// import { Demo10Pipe} from './demo10-pipe/demo10.pipe';
// import { Demo11Component } from './demo11-service/demo11.component';
// import { Demo12Component } from './demo12-customService/demo12.component';
// import { Demo13Component } from './demo13-http-service/demo13.component';
// import { Demo14Component } from './demo14-guard/demo14.component';
// import { Demo15Component } from './demo15-customDirective/demo15.component';
// import { TestDirective } from './demo15-customDirective/test.directive';
// import { ChangeBgColorDirective } from './demo15-customDirective/changeBgColor.directive';

import { HeaderComponent } from './angular-demo/header/header.component';
import { MainComponent } from './angular-demo/main/main.component';
import { InputBoxComponent } from './angular-demo/main/inputBox.component';
import { ToLoginComponent } from './angular-demo/login/login.component';
import { DetailComponent } from './angular-demo/detail/detail.component'; 
import { NotFoundComponent } from './angular-demo/not-found/not-found.component'; 
import { MailComponent } from './angular-demo/mail/mail.component';
import { InboxComponent } from './angular-demo/mail/inbox.component';
import { OutboxComponent } from './angular-demo/mail/outbox.component';
import { TrashComponent } from './angular-demo/mail/trash.component';

@NgModule({
  declarations: [
    AppComponent
    //,ChangeBgColorDirective,TestDirective,Demo15Component,Demo14Component,Demo13Component,Demo12Component,Demo11Component,Demo10Component,Demo10Pipe,Demo09ParentComponent,Demo09SonComponent ,OutboxComponent,InboxComponent, MailComponent, Demo07Component,DesComponent, StartComponent, Demo01Component , Demo02Component, Demo03LoginComponent, Demo04MainComponent, Demo05Component
    ,MailComponent,InboxComponent,OutboxComponent,TrashComponent,NotFoundComponent,DetailComponent,HeaderComponent,MainComponent,InputBoxComponent,ToLoginComponent
  ],
  imports: [
    BrowserModule, FormsModule , AppRoutingModule, HttpModule
  ],
  providers: [CanEnterGuard, CanCheckMailGuard ],
  bootstrap: [AppComponent]
})
export class AppModule { }
