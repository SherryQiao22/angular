import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myPipe'
})

export class Demo10Pipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    //必须有返回值
    if(value==1){
      return 'one';
    }else if(value==2){
      return 'two';
    }
  }
}