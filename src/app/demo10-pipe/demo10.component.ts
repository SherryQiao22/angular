import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'demo10',
  templateUrl: 'demo10.component.html'
})

export class Demo10Component implements OnInit {
  nowDate=new Date();
  stu={name:'zhangsan',age:20};
  stuName='Micheal';
  PI=3.1415;
  price=98.8;
  value=2.4;
  
  constructor() { }

  ngOnInit() { }
}