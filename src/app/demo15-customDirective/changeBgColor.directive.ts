import { Directive,Input,OnInit,ElementRef } from '@angular/core';

@Directive({ selector: '[changeBgColor]' })
export class ChangeBgColorDirective implements OnInit{
  constructor(private el:ElementRef) { }
  ngOnInit(){
    console.log(this.el.nativeElement);
    console.log(this.changeBgColor);
    this.el.nativeElement.style.backgoundColor=this.changeBgColor;
  }
  @Input() changeBgColor="";
  //@Input('changeBgColor') myColor:string; //重命名属性
}