import { Directive,OnInit,ElementRef } from '@angular/core';

@Directive({ selector: '[test]' })
export class TestDirective implements OnInit{
  constructor(private el:ElementRef) { }

  ngOnInit(){
    console.log("指令被调用了");
    console.log(this.el);//获得调用指令的元素
    this.el.nativeElement.innerHTML="Hello World";
  }
}