import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'demo15',
  template: `
    <h1 test>This is Demo15 Component.</h1>
    <h3 changeBgColor="#f00">custom directive</h3>
  `
})

export class Demo15Component implements OnInit {
  constructor() { }
  
  ngOnInit() { 

  }
}