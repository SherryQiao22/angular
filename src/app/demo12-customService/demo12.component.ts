import { Component, OnInit } from '@angular/core';
import { HeartBeatService } from './heartbeat.service';

@Component({
  selector: 'demo12',
  template: `<button (click)="startJump()">开始心跳</button>
             <button (click)="endJump()">结束心跳</button>
  `,
  providers: [HeartBeatService]
})

export class Demo12Component implements OnInit {
  constructor(private hbService:HeartBeatService) { }

  ngOnInit() { }
  startJump(){
    this.hbService.start();
  }
  endJump(){
    this.hbService.end();
  }
}