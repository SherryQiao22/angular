import { Injectable } from '@angular/core';

@Injectable()
export class HeartBeatService {
  t:any;
  isTimerWork:false;

  constructor() { }

  start(){
    if(this.isTimerWork==false){
      this.t=setInterval(()=>{
        console.log("正在心跳...");
      },500);
    }else{
      return ;
    }
  }
  end(){
    clearInterval(this.t);
    this.t=null;
  }
}