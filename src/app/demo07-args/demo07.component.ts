import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'demo07',
  template: '<h2>{{"收到的参数为"+this.myMsg}}</h2>'
})

export class Demo07Component implements OnInit {
  myMsg='';
  constructor(private aRoute:ActivatedRoute) { }

  ngOnInit() { 
    this.aRoute.params.subscribe((data)=>{
      this.myMsg=data.uname;
    });
  }
}