//创建自定义路由模块
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanEnterGuard } from './demo14-guard/canEnter.service';
import { CanCheckMailGuard } from './angular-demo/utilities/canCheckMail.service';

// import { Demo01Component } from './demo01-direction/demo01.component';
// import { Demo02Component } from './demo02-routing/demo02.component';
// import { Demo03LoginComponent } from './demo02-routing/demo03-login.component';
// import { Demo04MainComponent } from './demo02-routing/demo04-main.component';
// import { Demo05Component } from './demo05-jump/demo05.component';
// import { StartComponent } from './demo06-args/demo06-start.component';
// import { DesComponent } from './demo06-args/demo06-des.component';
// import { Demo07Component } from './demo07-args/demo07.component';
// import { MailComponent } from './demo08-embed/mail.component';
// import { InboxComponent } from './demo08-embed/inbox.component';
// import { OutboxComponent } from './demo08-embed/outbox.component';
// import { Demo09ParentComponent}  from './demo09-communication/demo09-parent.component';
// import { Demo10Component } from './demo10-pipe/demo10.component';
// import { Demo11Component } from './demo11-service/demo11.component';
// import { Demo12Component } from './demo12-customService/demo12.component';
// import { Demo13Component } from './demo13-http-service/demo13.component';
// import { Demo14Component } from './demo14-guard/demo14.component';
// import { Demo15Component } from './demo15-customDirective/demo15.component';

import { MainComponent } from './angular-demo/main/main.component';
import { ToLoginComponent } from './angular-demo/login/login.component'
import { DetailComponent } from './angular-demo/detail/detail.component'; 
import { NotFoundComponent } from './angular-demo/not-found/not-found.component'; 
import { MailComponent } from './angular-demo/mail/mail.component';
import { InboxComponent } from './angular-demo/mail/inbox.component';
import { OutboxComponent } from './angular-demo/mail/outbox.component';
import { TrashComponent } from './angular-demo/mail/trash.component';

const routes: Routes = [
  // { path: '', component: Demo05Component },
  // { path: 'demo01', component: Demo01Component },
  // { path: 'demo02', component: Demo02Component },
  // { path: 'login', component: Demo03LoginComponent },
  // { path: 'main', component: Demo04MainComponent },
  // { path: 'demo05', component: Demo05Component },
  // { path: 'start', component: StartComponent },
  // { path: 'des/:uname', component: DesComponent },
  // { path: 'demo07', component: Demo07Component },
  // { path: 'parent', component: Demo09ParentComponent },
  // { path: 'demo10', component: Demo10Component },
  // { path: 'demo11', component: Demo11Component },
  // { path: 'demo12', component: Demo12Component },
  // { path: 'demo13', component: Demo13Component },
  // { 
  //   path: 'demo14', 
  //   component: Demo14Component,
  //   canActivate:[CanEnterGuard] 
  // },
  // { path: 'demo15', component: Demo15Component },
  // { 
  //   path: 'mail', 
  //   component: MailComponent,
  //   children:[
  //     {path:'',component:InboxComponent},
  //     {path:'inbox',component:InboxComponent},
  //     {path:'outbox',component:OutboxComponent}
  //   ] 
  // },
  // { path: '**', component: Demo01Component}

  { path: '',redirectTo: '/main',pathMatch:'full' },
  { path: 'main', component: MainComponent },
  { path: 'toLogin', component: ToLoginComponent },
  { 
    path: 'detail', 
    component: DetailComponent,
    canActivate: [CanCheckMailGuard]
  },
  { 
    path: 'mail', 
    component: MailComponent,
    children:[
      {path:'',component:InboxComponent},
      {path:'inbox',component:InboxComponent},
      {path:'outbox',component:OutboxComponent},
      {path:'trash',component:TrashComponent}
    ]
  },


  { path: '**',component:NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
