import { Component, OnInit } from '@angular/core';
import { MyHttpService } from './myhttp.service';

@Component({
  selector: 'demo13',
  template: `
    <h1>This is Demo13 Component.</h1>
    <button (click)="getData()">发起请求</button>
  `,
  providers:[MyHttpService]
})

export class Demo13Component implements OnInit {
  constructor(private httpService:MyHttpService) { }

  ngOnInit() { }

  getData(){
    //调用网络请求服务类中的sendRequest方法
    this.httpService
        .sendRequest("https://jsonplaceholder.typicode.com/posts")
        .subscribe((data:any)=>{
          console.log(data);
        });
  }
}